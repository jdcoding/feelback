package com.johndoe_software.feelback

import com.johndoe_software.feelback.ui.statistics.daysSinceToday
import com.johndoe_software.feelback.ui.statistics.milliSecondsPerDay
import com.johndoe_software.feelback.ui.statistics.todayMidnight
import org.junit.Assert.assertEquals
import org.junit.Test

class DaysSinceTodayTest {
    @Test
    fun todayFirstSecond() {
        val todayFirstMillisecond = todayMidnight() + 1000
        assertEquals(0, daysSinceToday(todayFirstMillisecond))
    }

    @Test
    fun todayLastSecond() {
        val todayLastMillisecond = todayMidnight() + milliSecondsPerDay - 1000
        assertEquals(0, daysSinceToday(todayLastMillisecond))
    }

    @Test
    fun yesterdayLastSecond() {
        val yesterdayLastMillisecond = todayMidnight() - 1000
        assertEquals(-1, daysSinceToday(yesterdayLastMillisecond))
    }

    @Test
    fun yesterdayFirstSecond() {
        val yesterdayFirstMillisecond = todayMidnight() - milliSecondsPerDay + 1000
        assertEquals(-1, daysSinceToday(yesterdayFirstMillisecond))
    }
}
