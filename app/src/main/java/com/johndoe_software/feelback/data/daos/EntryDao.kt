package com.johndoe_software.feelback.data.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.johndoe_software.feelback.data.Entry
import com.johndoe_software.feelback.data.EntryWithSubEntriesAndFeelings
import java.sql.Timestamp

@Dao
interface EntryDao {
    @Transaction
    @Query("SELECT * from entries ORDER BY time_stamp DESC")
    fun getSortedEntries(): LiveData<List<EntryWithSubEntriesAndFeelings>>

    @Transaction
    @Query("SELECT * from entries WHERE time_stamp > :minTimeStamp ORDER BY time_stamp DESC")
    fun getFilteredEntries(minTimeStamp: Timestamp): List<EntryWithSubEntriesAndFeelings>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(entry: Entry): Long
}
