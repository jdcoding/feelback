package com.johndoe_software.feelback.data.daos

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

val MIGRATION_1_2 =
    object : Migration(1, 2) {
        override fun migrate(db: SupportSQLiteDatabase) {
            db.execSQL("DELETE FROM sub_entries WHERE intensity = 0")
        }
    }

val MIGRATION_2_3 =
    object : Migration(2, 3) {
        override fun migrate(db: SupportSQLiteDatabase) {
            db.execSQL("ALTER TABLE feelings ADD COLUMN sub_entry_category STRING")
            db.execSQL("UPDATE feelings SET sub_entry_category = FEELING")
        }
    }

val MIGRATION_3_4 =
    object : Migration(3, 4) {
        override fun migrate(db: SupportSQLiteDatabase) {
            val categoriesInit =
                "feeling_id         INTEGER PRIMARY KEY NOT NULL, " +
                    "name               TEXT    NOT NULL, " +
                    "sub_entry_category TEXT    NOT NULL, " +
                    "visible            INTEGER NOT NULL"
            val categoriesToCopy = "feeling_id, name, sub_entry_category, visible"
            val table = "feelings"
            val backupTable = "${table}_backup "

            db.execSQL("CREATE TABLE $backupTable($categoriesInit)")
            db.execSQL("INSERT INTO $backupTable SELECT $categoriesToCopy FROM $table")
            db.execSQL("DROP TABLE $table")

            db.execSQL("CREATE TABLE $table($categoriesInit)")
            db.execSQL("INSERT INTO $table SELECT $categoriesToCopy FROM $backupTable")
            db.execSQL("DROP TABLE $backupTable")
        }
    }

val MIGRATION_4_5 =
    object : Migration(4, 5) {
        override fun migrate(db: SupportSQLiteDatabase) {
            db.execSQL("ALTER TABLE entries ADD COLUMN note TEXT NOT NULL DEFAULT ''")
        }
    }
