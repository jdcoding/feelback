package com.johndoe_software.feelback.data

import androidx.lifecycle.LiveData
import com.johndoe_software.feelback.data.daos.FeelingDao
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class FeelingRepository(
    private val subEntryDao: FeelingDao,
    private val ioDispatcher: CoroutineDispatcher
) {
    val allSubEntries: LiveData<List<SubEntry>> = subEntryDao.getSortedSubEntries()

    suspend fun insert(subEntry: SubEntry): Long {
        return subEntryDao.insert(subEntry)
    }

    suspend fun updateVisible(feelingID: Long, visible: Boolean) {
        withContext(ioDispatcher) {
            subEntryDao.updateVisible(feelingID, visible)
        }
    }

    suspend fun feelingWithStringAndCategory(
        inputText: String,
        category: SubEntryCategory
    ): SubEntry? {
        return subEntryDao.subEntry(inputText, category.name)
    }

    suspend fun updateName(subEntryID: Long, newName: String) {
        withContext(ioDispatcher) {
            subEntryDao.updateName(subEntryID, newName)
        }
    }
}
