package com.johndoe_software.feelback.data

enum class SubEntryCategory {
    FEELING,
    ACTIVITY,
    NEED,
}
