package com.johndoe_software.feelback.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "feelings")
data class SubEntry(
    @ColumnInfo(name = "feeling_id") @PrimaryKey(autoGenerate = true)
    val subEntryID: Long,
    var name: String,
    @ColumnInfo(name = "sub_entry_category")
    val category: SubEntryCategory,
    var visible: Boolean
)
