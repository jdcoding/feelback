package com.johndoe_software.feelback.data.daos

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.CoroutineWorker
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import com.johndoe_software.feelback.R
import com.johndoe_software.feelback.data.Entry
import com.johndoe_software.feelback.data.SubEntry
import com.johndoe_software.feelback.data.SubEntryCategory
import com.johndoe_software.feelback.data.SubEntryWithEntryID
import kotlinx.coroutines.coroutineScope

@Database(
    entities = [Entry::class, SubEntry::class, SubEntryWithEntryID::class],
    version = 5,
    exportSchema = true,
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun entryDao(): EntryDao

    abstract fun subEntryDao(): SubEntryDao

    abstract fun feelingDao(): FeelingDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance: AppDatabase? =
                INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance =
                    Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "feelback-db",
                    )
                        .addCallback(FeelingDatabaseCallback(context = context))
                        .addMigrations(MIGRATION_1_2)
                        .addMigrations(MIGRATION_2_3)
                        .addMigrations(MIGRATION_3_4)
                        .addMigrations(MIGRATION_4_5)
                        .build()
                INSTANCE = instance
                return instance
            }
        }

        private class FeelingDatabaseCallback(
            private val context: Context,
        ) : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                val request =
                    OneTimeWorkRequestBuilder<SeedDatabaseWorker>().build()
                WorkManager.getInstance(context).enqueue(request)
            }
        }
    }
}

class SeedDatabaseWorker(
    private val context: Context,
    workerParams: WorkerParameters,
) : CoroutineWorker(context, workerParams) {
    override suspend fun doWork(): Result =
        coroutineScope {
            val database = AppDatabase.getDatabase(applicationContext)
            val feelings = initFeelings(context)
            val activities = initActivities(context)
            (feelings + activities).forEach {
                database.feelingDao().insert(it)
            }
            Result.success()
        }
}

fun initFeelings(context: Context): List<SubEntry> {
    val strings =
        listOf(
            R.string.good,
            R.string.bad,
            R.string.happy,
            R.string.sad,
            R.string.grateful,
            R.string.angry,
            R.string.loved,
            R.string.anxious,
            R.string.confident,
            R.string.guilty,
            R.string.authentic,
            R.string.lonely,
            R.string.relieved,
            R.string.relaxed,
            R.string.surprised,
        )
    return stringToSubEntry(strings, context, SubEntryCategory.FEELING)
}

fun initActivities(context: Context): List<SubEntry> {
    val strings =
        listOf(
            R.string.sleep,
            R.string.eat,
            R.string.social,
            R.string.friends,
            R.string.family,
            R.string.work,
            R.string.commute,
            R.string.household,
            R.string.shopping,
            R.string.leisure,
            R.string.sports,
        )
    return stringToSubEntry(strings, context, SubEntryCategory.ACTIVITY)
}

private fun stringToSubEntry(
    strings: List<Int>,
    context: Context,
    category: SubEntryCategory,
): List<SubEntry> {
    return strings.map { string ->
        getSubEntry(
            name = context.getString(string),
            category = category,
        )
    }
}

fun getSubEntry(
    name: String,
    category: SubEntryCategory,
): SubEntry {
    return SubEntry(
        subEntryID = 0,
        name = name,
        category = category,
        visible = true,
    )
}
