package com.johndoe_software.feelback.data

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class FeelingWithEntriesAndSubEntries(
    @Embedded val subEntry: SubEntry,
    @Relation(
        parentColumn = "feeling_id",
        entityColumn = "entry_id",
        associateBy = Junction(SubEntryWithEntryID::class)
    )
    val entries: List<Entry>,
    @Relation(
        parentColumn = "feeling_id",
        entityColumn = "feeling_id"
    )
    val subEntryWithEntryIDs: List<SubEntryWithEntryID>
)
