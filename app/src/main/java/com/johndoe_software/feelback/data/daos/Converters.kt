package com.johndoe_software.feelback.data.daos

import androidx.room.TypeConverter
import java.sql.Timestamp

class Converters {
    @TypeConverter
    fun longToTimestamp(value: Long?): Timestamp? {
        return value?.let { Timestamp(it) }
    }

    @TypeConverter
    fun timestampToLong(date: Timestamp?): Long? {
        return date?.time
    }
}
