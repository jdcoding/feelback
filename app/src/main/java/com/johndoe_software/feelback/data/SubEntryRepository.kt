package com.johndoe_software.feelback.data

import com.johndoe_software.feelback.data.daos.SubEntryDao
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class SubEntryRepository(
    private val subEntryDao: SubEntryDao,
    private val ioDispatcher: CoroutineDispatcher
) {
    suspend fun insert(subEntriesWithEntryID: List<SubEntryWithEntryID>) {
        withContext(ioDispatcher) {
            subEntryDao.insert(subEntriesWithEntryID)
        }
    }
}
