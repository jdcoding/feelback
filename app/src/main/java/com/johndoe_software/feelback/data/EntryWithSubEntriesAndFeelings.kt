package com.johndoe_software.feelback.data

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class EntryWithSubEntriesAndFeelings(
    @Embedded val entry: Entry,
    @Relation(
        parentColumn = "entry_id",
        entityColumn = "entry_id"
    )
    val subEntryWithEntryIDs: List<SubEntryWithEntryID>,
    @Relation(
        parentColumn = "entry_id",
        entityColumn = "feeling_id",
        associateBy = Junction(SubEntryWithEntryID::class)
    )
    val subEntries: List<SubEntry>
)
