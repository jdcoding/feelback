package com.johndoe_software.feelback.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Timestamp

@Entity(tableName = "entries")
data class Entry(
    @ColumnInfo(name = "entry_id") @PrimaryKey(autoGenerate = true)
    val entryID: Long,
    @ColumnInfo(name = "time_stamp") val timeStamp: Timestamp,
    @ColumnInfo(name = "note", defaultValue = "") val note: String,
)
