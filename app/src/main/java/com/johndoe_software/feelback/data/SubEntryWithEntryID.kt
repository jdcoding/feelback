package com.johndoe_software.feelback.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "sub_entries", indices = [Index("feeling_id")])
data class SubEntryWithEntryID(
    @ColumnInfo(name = "sub_entry_id") @PrimaryKey(autoGenerate = true) val subEntryID: Long,
    @ColumnInfo(name = "entry_id") val entryID: Long,
    @ColumnInfo(name = "feeling_id") val feelingID: Long,
    val intensity: Float,
    val isDesired: Boolean,
)
