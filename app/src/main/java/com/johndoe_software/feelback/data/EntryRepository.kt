package com.johndoe_software.feelback.data

import androidx.lifecycle.LiveData
import com.johndoe_software.feelback.data.daos.EntryDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.sql.Timestamp

class EntryRepository(private val entryDao: EntryDao) {
    val allEntries: LiveData<List<EntryWithSubEntriesAndFeelings>> = entryDao.getSortedEntries()

    suspend fun insert(entry: Entry): Long {
        return entryDao.insert(entry)
    }

    suspend fun getFilteredEntries(minTimeStamp: Timestamp): List<EntryWithSubEntriesAndFeelings> {
        return withContext(Dispatchers.IO) {
            entryDao.getFilteredEntries(minTimeStamp)
        }
    }
}
