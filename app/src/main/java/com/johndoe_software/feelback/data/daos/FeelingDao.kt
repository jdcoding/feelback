package com.johndoe_software.feelback.data.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.johndoe_software.feelback.data.SubEntry

@Dao
interface FeelingDao {
    @Transaction
    @Query("SELECT * from feelings ORDER BY feeling_id ASC")
    fun getSortedSubEntries(): LiveData<List<SubEntry>>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insert(subEntry: SubEntry): Long

    @Query("UPDATE feelings SET visible = :visible WHERE feeling_id = :feelingID")
    suspend fun updateVisible(feelingID: Long, visible: Boolean)

    @Query(
        "SELECT * from feelings " +
            "WHERE name = :feelingString " +
            "AND sub_entry_category = :category"
    )
    suspend fun subEntry(feelingString: String, category: String): SubEntry?

    @Query("UPDATE feelings SET name = :newName WHERE feeling_id = :subEntryID")
    fun updateName(subEntryID: Long, newName: String)
}
