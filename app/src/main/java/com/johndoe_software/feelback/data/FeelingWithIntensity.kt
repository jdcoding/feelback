package com.johndoe_software.feelback.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FeelingWithIntensity(
    val feelingID: Long,
    var intensity: Float,
    val isDesired: Boolean,
) : Parcelable
