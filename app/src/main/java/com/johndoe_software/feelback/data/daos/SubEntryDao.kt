package com.johndoe_software.feelback.data.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.johndoe_software.feelback.data.SubEntryWithEntryID

@Dao
interface SubEntryDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(subEntriesWithEntryID: List<SubEntryWithEntryID>)
}
