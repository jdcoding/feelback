package com.johndoe_software.feelback.ui.entries

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.dp
import com.johndoe_software.feelback.R
import com.johndoe_software.feelback.data.SubEntry
import com.johndoe_software.feelback.ui.commonCompose.FeelBackChip
import com.johndoe_software.feelback.ui.commonCompose.SubEntryNameFromDialog

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun NewEntryChipGroup(
    selected: List<Long>,
    onChipClicked: (Long) -> Unit,
    subEntries: List<SubEntry>,
    onPlusButtonClick: (String) -> Unit,
    newSubEntryDialogTitle: String,
) {
    val newSubEntryDialogIsVisible = remember { mutableStateOf(false) }
    if (newSubEntryDialogIsVisible.value) {
        SubEntryNameFromDialog(newSubEntryDialogTitle, "", onPlusButtonClick) {
            newSubEntryDialogIsVisible.value = false
        }
    }
    FlowRow(
        horizontalArrangement = Arrangement.spacedBy(10.dp),
        verticalArrangement = Arrangement.spacedBy(10.dp),
        modifier =
            Modifier.padding(
                top = dimensionResource(id = R.dimen.padding_small),
                bottom = dimensionResource(id = R.dimen.padding_small),
            ),
    ) {
        subEntries.forEach { it ->
            FeelBackChip(
                name = it.name,
                subEntryID = it.subEntryID,
                onClick = { onChipClicked(it.subEntryID) },
                selected = it.subEntryID in selected,
            )
        }

        FilterChip(label = { Text(text = " + ") }, selected = false, onClick = {
            newSubEntryDialogIsVisible.value = true
        }, modifier = Modifier.height(FilterChipDefaults.Height))
    }
}
