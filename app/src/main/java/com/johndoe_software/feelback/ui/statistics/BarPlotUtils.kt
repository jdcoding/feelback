package com.johndoe_software.feelback.ui.statistics

import java.util.Calendar

const val milliSecondsPerDay = 1000 * 3600 * 24

fun daysSinceToday(unixTimeInMilliseconds: Long): Long {
    val diffMilliseconds = unixTimeInMilliseconds - todayMidnight()
    return kotlin.math.floor(diffMilliseconds.toDouble() / milliSecondsPerDay).toLong()
}

fun todayMidnight(): Long {
    val calendar = Calendar.getInstance()
    val todayMilliseconds =
        (
            calendar.get(Calendar.HOUR_OF_DAY) * 3600 +
                calendar.get(Calendar.MINUTE) * 60 +
                calendar.get(Calendar.SECOND)
        ) * 1000 + calendar.get(Calendar.MILLISECOND)
    return calendar.time.time - todayMilliseconds
}
