package com.johndoe_software.feelback.ui.statistics

import android.view.LayoutInflater
import android.widget.Button
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ExpandLess
import androidx.compose.material.icons.rounded.ExpandMore
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewmodel.compose.viewModel
import com.johndoe_software.feelback.R
import com.johndoe_software.feelback.data.EntryWithSubEntriesAndFeelings
import com.johndoe_software.feelback.data.FeelingWithEntriesAndSubEntries
import com.johndoe_software.feelback.ui.EntryViewModel
import com.johndoe_software.feelback.ui.commonCompose.FeelBackChip
import com.johndoe_software.feelback.ui.commonCompose.PossibleIntervals
import com.johndoe_software.feelback.ui.commonCompose.entriesToSubEntries
import com.johndoe_software.feelback.ui.commonCompose.setInterval

fun getIconDependingOnState(expanded: Boolean): ImageVector {
    return if (expanded) {
        Icons.Rounded.ExpandLess
    } else {
        Icons.Rounded.ExpandMore
    }
}

@Suppress("ktlint:standard:function-naming")
@Composable
fun SubEntryContent(
    entries: List<EntryWithSubEntriesAndFeelings>,
    subEntries: List<FeelingWithEntriesAndSubEntries>,
    ix: Int,
    days: Int,
) {
    var statsVisible by rememberSaveable { mutableStateOf(false) }

    Column(
        modifier =
            Modifier.padding(horizontal = dimensionResource(R.dimen.padding_small)),
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            FeelBackChip(
                name = subEntries[ix].subEntry.name + ": " + subEntries[ix].entries.size.toString(),
                subEntryID = subEntries[ix].subEntry.subEntryID,
                onClick = {},
                selected = false,
            )

            Box(modifier = Modifier.weight(1f))

            IconButton(
                onClick = {
                    statsVisible = !statsVisible
                },
            ) {
                Icon(
                    getIconDependingOnState(statsVisible),
                    contentDescription = "Expand more",
                )
            }
        }
        ExtendableStatisticsCard(statsVisible, entries, subEntries, ix, days)
    }
}

@Composable
fun <T> LazyCardColumn(
    subEntries: List<T>,
    modifier: Modifier,
    composable: @Composable (List<T>, Int) -> Unit,
) {
    val scrollState = rememberLazyListState()
    LazyColumn(
        state = scrollState,
        verticalArrangement =
            Arrangement.spacedBy(
                dimensionResource(R.dimen.padding_small),
            ),
        contentPadding =
            PaddingValues(
                horizontal = dimensionResource(R.dimen.padding_small),
                vertical = dimensionResource(R.dimen.padding_small),
            ),
        modifier = modifier,
    ) {
        items(subEntries.size) { ix ->
            Card(modifier = Modifier.fillMaxWidth()) {
                composable(subEntries, ix)
            }
        }
    }
}

val intervalToDays =
    mapOf(
        PossibleIntervals.MONTH to 30,
        PossibleIntervals.QUARTER to 91,
        PossibleIntervals.YEAR to 365,
    )

@Suppress("ktlint:standard:function-naming")
@Composable
fun IntervalSelector(
    interval: PossibleIntervals,
    onToggleFn: (PossibleIntervals) -> Unit,
) {
    Card(
        elevation = CardDefaults.cardElevation(2.dp),
        shape = RoundedCornerShape(0),
        modifier = Modifier.shadow(8.dp),
        colors = CardDefaults.cardColors(MaterialTheme.colorScheme.surface),
    ) {
        AndroidView(
            modifier =
                Modifier
                    .fillMaxWidth()
                    .padding(dimensionResource(R.dimen.padding_small)),
            factory = { context ->
                val view =
                    LayoutInflater.from(context).inflate(R.layout.interval_selector, null, false)
                val allTimeButton = view.findViewById<Button>(R.id.yearButton)
                allTimeButton.setOnClickListener { onToggleFn(PossibleIntervals.YEAR) }
                val quarterButton = view.findViewById<Button>(R.id.quarterButton)
                quarterButton.setOnClickListener { onToggleFn(PossibleIntervals.QUARTER) }
                val monthButton = view.findViewById<Button>(R.id.monthButton)
                monthButton.setOnClickListener { onToggleFn(PossibleIntervals.MONTH) }
                onToggleFn(interval)
                view
            },
            update = {
                // Update the view
            },
        )
    }
}

@Suppress("ktlint:standard:function-naming")
@Composable
fun StatisticsScreen() {
    val entryViewModel: EntryViewModel = viewModel()
    var interval by rememberSaveable { mutableStateOf(PossibleIntervals.MONTH) }

    Column {
        val filteredEntries = entryViewModel.allEntriesFiltered.observeAsState().value
        val subEntries = filteredEntries?.let { entriesToSubEntries(it) }?.filter { it.subEntry.visible }

        val sortedSubEntries =
            subEntries?.sortedWith(
                compareByDescending<FeelingWithEntriesAndSubEntries> {
                    it.entries.size
                }.thenBy {
                    it.subEntry.name
                },
            )

        sortedSubEntries?.let {
            LazyCardColumn(it, modifier = Modifier.weight(1f)) { subEntries, ix ->
                Card(
                    modifier = Modifier.fillMaxWidth(),
                    colors = CardDefaults.cardColors(MaterialTheme.colorScheme.surfaceVariant),
                ) {
                    SubEntryContent(filteredEntries, subEntries, ix, days = intervalToDays[interval]!!)
                }
            }
        }

        IntervalSelector(interval) {
            interval = it
            setInterval(interval, entryViewModel)
        }
    }
}
