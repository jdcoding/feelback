package com.johndoe_software.feelback.ui.entries

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.johndoe_software.feelback.R
import com.johndoe_software.feelback.data.FeelingWithIntensity
import com.johndoe_software.feelback.data.SubEntry
import com.johndoe_software.feelback.data.SubEntryCategory
import com.johndoe_software.feelback.ui.commonCompose.AppTheme
import com.johndoe_software.feelback.ui.commonCompose.SubEntryChipGroup
import com.johndoe_software.feelback.ui.commonCompose.labelFromIntensity

@Composable
fun EntriesScreenItem(
    timeStamp: String,
    subEntries: List<Pair<SubEntry, FeelingWithIntensity>>,
    note: String,
) {
    Card(colors = CardDefaults.cardColors(MaterialTheme.colorScheme.surfaceVariant)) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(
                    dimensionResource(R.dimen.fab_margin),
                    dimensionResource(R.dimen.padding_small),
                ),
        ) {
            Text(
                text = timeStamp,
                style = MaterialTheme.typography.bodySmall,
                modifier = Modifier.padding(top = dimensionResource(R.dimen.padding_small)),
            )
            val visibleSubEntries = subEntries.filter { it.first.visible }
            EntriesScreenChipGroup(
                title = stringResource(id = R.string.activities),
                subEntries =
                    visibleSubEntries.filter {
                        it.first.category == SubEntryCategory.ACTIVITY && !it.second.isDesired
                    }.map {
                        Pair(it.first.name, it.first.subEntryID)
                    },
            )
            EntriesScreenChipGroup(
                title = stringResource(id = R.string.feelings),
                subEntries =
                    visibleSubEntries.filter {
                        it.first.category == SubEntryCategory.FEELING && !it.second.isDesired
                    }.map {
                        val label =
                            if (it.second.intensity == 1f) {
                                it.first.name
                            } else {
                                labelFromIntensity(it.first.name, it.second.intensity)
                            }
                        Pair(label, it.first.subEntryID)
                    },
            )
            EntriesScreenChipGroup(
                title = stringResource(id = R.string.desired_feelings),
                subEntries =
                    visibleSubEntries.filter {
                        it.first.category == SubEntryCategory.FEELING && it.second.isDesired
                    }.map { Pair(it.first.name, it.first.subEntryID) },
            )

            if (note != "") {
                Text(
                    text = stringResource(R.string.note),
                    style = MaterialTheme.typography.titleMedium,
                    modifier = Modifier.padding(top = dimensionResource(id = R.dimen.padding_small)),
                )
                Text(
                    text = note,
                    style = MaterialTheme.typography.bodyMedium,
                    modifier = Modifier.padding(bottom = dimensionResource(id = R.dimen.padding_small)),
                )
            }
        }
    }
}

@Suppress("ktlint:standard:function-naming")
@Composable
fun EntriesScreenChipGroup(
    title: String,
    subEntries: List<Pair<String, Long>>,
) {
    if (subEntries.isNotEmpty()) {
        Column(modifier = Modifier.padding(top = dimensionResource(R.dimen.padding_small))) {
            Text(
                text = "$title:",
                style = MaterialTheme.typography.titleMedium,
            )
            SubEntryChipGroup(subEntries = subEntries)
        }
    }
}

@Preview
@Composable
fun ComposablePreview() {
    AppTheme {
        EntriesScreenItem(
            "October 18th, 2021",
            listOf(
                Pair(
                    SubEntry(
                        1,
                        "Happy",
                        SubEntryCategory.FEELING,
                        true,
                    ),
                    FeelingWithIntensity(
                        1,
                        0.5f,
                        true,
                    ),
                ),
                Pair(
                    SubEntry(
                        2,
                        "Angry",
                        SubEntryCategory.FEELING,
                        true,
                    ),
                    FeelingWithIntensity(
                        2,
                        0.5f,
                        false,
                    ),
                ),
                Pair(
                    SubEntry(
                        3,
                        "Sad",
                        SubEntryCategory.FEELING,
                        true,
                    ),
                    FeelingWithIntensity(
                        3,
                        1f,
                        true,
                    ),
                ),
            ),
            note = "Test",
        )
    }
}

@Preview
@Composable
fun SuggestedActivityChipGroup() {
    AppTheme {
        Card(colors = CardDefaults.cardColors(MaterialTheme.colorScheme.surfaceVariant)) {
            EntriesScreenChipGroup(
                title = "Activities maximizing the chance for desired feelings",
                subEntries =
                    listOf(
                        Pair("Walk: 75%", 1),
                        Pair("Read: 68%", 2),
                        Pair("Work: 54%", 3),
                    ),
            )
        }
    }
}
