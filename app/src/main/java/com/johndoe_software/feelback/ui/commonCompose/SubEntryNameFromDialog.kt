package com.johndoe_software.feelback.ui.commonCompose

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import com.johndoe_software.feelback.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SubEntryNameFromDialog(
    title: String,
    initialText: String,
    onConfirmFn: (String) -> Unit,
    onDismissFn: () -> Unit
) {
    var text by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue(initialText))
    }
    AlertDialog(
        onDismissRequest = onDismissFn,
        confirmButton = {
            TextButton(
                onClick = {
                    onDismissFn()
                    onConfirmFn(text.text)
                }
            ) {
                Text("Ok")
            }
        },
        dismissButton = {
            TextButton(
                onClick = {
                    onDismissFn()
                }
            ) {
                Text(stringResource(R.string.cancel))
            }
        },
        title = { Text(title) },
        text = {
            TextField(
                value = text,
                onValueChange = { text = it }
            )
        }
    )
}
