package com.johndoe_software.feelback.ui.commonCompose

import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.dp
import com.johndoe_software.feelback.R
import java.math.RoundingMode

val lightRainbowColors =
    listOf(
        light_darkBlue,
        light_lightBlue,
        light_purple,
        light_pink,
        light_red,
        light_orange,
        light_yellow,
        light_lightGreen,
        light_darkGreen,
    )

val lightOnRainbowColors =
    listOf(
        light_ondarkBlue,
        light_onlightBlue,
        light_onpurple,
        light_onpink,
        light_onred,
        light_onorange,
        light_onyellow,
        light_onlightGreen,
        light_ondarkGreen,
    )

val darkRainbowColors =
    listOf(
        dark_darkBlue,
        dark_lightBlue,
        dark_purple,
        dark_pink,
        dark_red,
        dark_orange,
        dark_yellow,
        dark_lightGreen,
        dark_darkGreen,
    )

val darkOnRainbowColors =
    listOf(
        dark_ondarkBlue,
        dark_onlightBlue,
        dark_onpurple,
        dark_onpink,
        dark_onred,
        dark_onorange,
        dark_onyellow,
        dark_onlightGreen,
        dark_ondarkGreen,
    )

fun colorFromSubEntryID(
    id: Long,
    palette: List<Color>,
): Color {
    val colorNo = (id - 1) % palette.size
    return palette[colorNo.toInt()]
}

@Composable
fun getPalettes(): Pair<List<Color>, List<Color>> {
    return when (isSystemInDarkTheme()) {
        false -> Pair(lightRainbowColors, lightOnRainbowColors)
        true -> Pair(darkRainbowColors, darkOnRainbowColors)
    }
}

@Suppress("ktlint:standard:function-naming")
@Composable
fun FeelBackChip(
    name: String,
    subEntryID: Long,
    onClick: () -> Unit,
    selected: Boolean,
) {
    val (backgroundPalette, textPalette) = getPalettes()
    val brush =
        Brush.linearGradient(
            colors = backgroundPalette,
        )

    val backgroundColor =
        when (subEntryID) {
            1L -> {
                Color.Transparent
            }
            2L -> {
                Color.Black
            }
            else -> {
                colorFromSubEntryID(subEntryID, backgroundPalette)
            }
        }
    val textColor =
        when (subEntryID) {
            1L -> {
                when (isSystemInDarkTheme()) {
                    false -> Color.White
                    true -> Color.Black
                }
            }

            2L -> {
                Color.White
            }

            else -> {
                colorFromSubEntryID(subEntryID, textPalette)
            }
        }

    FilterChip(
        label = { Text(text = name) },
        selected = true,
        modifier =
            Modifier
                .background(
                    brush = brush,
                    shape = FilterChipDefaults.shape,
                ).height(FilterChipDefaults.Height),
        onClick = onClick,
        colors =
            FilterChipDefaults.filterChipColors(
                containerColor = backgroundColor,
                selectedContainerColor = backgroundColor,
                labelColor = textColor,
                selectedLabelColor = textColor,
                selectedLeadingIconColor = textColor,
            ),
        leadingIcon =
            if (selected) {
                {
                    Icon(
                        imageVector = Icons.Filled.Done,
                        contentDescription = "Localized Description",
                        modifier = Modifier.size(FilterChipDefaults.IconSize),
                    )
                }
            } else {
                null
            },
    )
}

@Suppress("ktlint:standard:function-naming")
@OptIn(ExperimentalLayoutApi::class)
@Composable
fun SubEntryChipGroup(subEntries: List<Pair<String, Long>>) {
    FlowRow(
        horizontalArrangement = Arrangement.spacedBy(10.dp),
        verticalArrangement = Arrangement.spacedBy(10.dp),
        modifier =
            Modifier.padding(
                top = dimensionResource(R.dimen.padding_small),
                bottom = dimensionResource(R.dimen.padding_small),
            ),
    ) {
        subEntries.forEach {
            FeelBackChip(name = it.first, subEntryID = it.second, onClick = {}, selected = false)
        }
    }
}

fun labelFromIntensity(
    name: String,
    intensity: Float,
): String {
    return name + ": " + (intensity * 100).toBigDecimal().setScale(0, RoundingMode.HALF_UP) + "%"
}
