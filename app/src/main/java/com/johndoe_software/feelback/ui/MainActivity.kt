package com.johndoe_software.feelback.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.github.doyaaaaaken.kotlincsv.dsl.csvWriter
import com.johndoe_software.feelback.data.EntryWithSubEntriesAndFeelings
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

fun exportTable(activity: Activity, entries: List<EntryWithSubEntriesAndFeelings>) {
    val zipFile = getZipFile(activity.filesDir, entries)

    val contentUri = FileProvider.getUriForFile(
        activity,
        "${activity.packageName}.file_provider",
        zipFile
    )
    val mimeType = activity.contentResolver.getType(contentUri)
    val shareIntent: Intent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_STREAM, contentUri)
        type = mimeType
        flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
    }

    activity.startActivity(shareIntent)
}

private fun getZipFile(
    filesDir: File,
    entries: List<EntryWithSubEntriesAndFeelings>
): File {
    val files = getFiles(filesDir, entries)

    val zipFile = File(filesDir, "data.zip")
    ZipOutputStream(BufferedOutputStream(FileOutputStream(zipFile))).use { zos ->
        files.forEach { file ->
            val zipFileName = file.absolutePath.removePrefix(
                filesDir.absolutePath
            ).removePrefix("/")
            val zipEntry = ZipEntry(zipFileName)
            zos.putNextEntry(zipEntry)
            file.inputStream().copyTo(zos)
        }
    }
    return zipFile
}

private fun getFiles(
    filesDir: File,
    entries: List<EntryWithSubEntriesAndFeelings>
): List<File> {
    val entryFile = File(filesDir, "entries.csv")
    entryFile.createNewFile()
    val subEntryFile = File(filesDir, "sub-entries.csv")
    subEntryFile.createNewFile()
    val subEntryWithEntryIDFile = File(filesDir, "sub-entries-with-entry-ids.csv")
    subEntryWithEntryIDFile.createNewFile()

    csvWriter().open(entryFile) {
        writeRow(listOf("entry_id", "time_stamp"))
        writeRows(entries.map { listOf(it.entry.entryID, it.entry.timeStamp) })
    }
    csvWriter().open(subEntryFile) {
        writeRow(listOf("sub_entry_id", "name", "category", "visible"))
        writeRows(
            entries.flatMap { entry ->
                entry.subEntries.map { subEntry ->
                    listOf(subEntry.subEntryID, subEntry.name, subEntry.category, subEntry.visible)
                }
            }.distinct()
        )
    }

    csvWriter().open(subEntryWithEntryIDFile) {
        writeRow(listOf("sub_entry_id", "entry_id"))
        writeRows(
            entries.flatMap { entry ->
                entry.subEntries.map { subEntry ->
                    listOf(subEntry.subEntryID, entry.entry.entryID)
                }
            }.distinct()
        )
    }
    return listOf(entryFile, subEntryFile, subEntryWithEntryIDFile)
}

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MainActivityLayout { entries: List<EntryWithSubEntriesAndFeelings> ->
                exportTable(this, entries)
            }
        }
    }
}
