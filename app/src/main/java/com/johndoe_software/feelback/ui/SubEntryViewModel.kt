package com.johndoe_software.feelback.ui

import android.app.Application
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.johndoe_software.feelback.data.FeelingRepository
import com.johndoe_software.feelback.data.SubEntry
import com.johndoe_software.feelback.data.SubEntryCategory
import com.johndoe_software.feelback.data.daos.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SubEntryViewModel(application: Application) :
    AndroidViewModel(application) {
    private val repository: FeelingRepository
    val allSubEntries: LiveData<List<SubEntry>>
    var checkedSubEntryIDs = mutableStateListOf<Pair<Long, Boolean>>()
    var goodIntensity = mutableFloatStateOf(0.5f)

    init {
        val feelingsDao = AppDatabase.getDatabase(application).feelingDao()
        repository = FeelingRepository(feelingsDao, Dispatchers.IO)
        allSubEntries = repository.allSubEntries
    }

    fun addFeelingToListIfNotExisting(
        subEntryString: String,
        subEntryCategory: SubEntryCategory,
        alreadyExistingCallback: () -> Unit,
    ) {
        viewModelScope.launch {
            val existingFeeling =
                repository.feelingWithStringAndCategory(subEntryString, subEntryCategory)
            if (existingFeeling == null) {
                createAndInsertFeeling(subEntryString, subEntryCategory)
            } else {
                alreadyExistingCallback()
            }
        }
    }

    private suspend fun createAndInsertFeeling(
        feelingString: String,
        subEntryCategory: SubEntryCategory,
    ) {
        val feeling =
            SubEntry(
                subEntryID = 0,
                name = feelingString,
                category = subEntryCategory,
                visible = true,
            )
        repository.insert(feeling)
    }

    fun updateVisible(
        feelingID: Long,
        visible: Boolean,
    ) = viewModelScope.launch {
        repository.updateVisible(feelingID = feelingID, visible = visible)
    }

    fun updateNameIfNotExisting(
        subEntryID: Long,
        newName: String,
        subEntryCategory: SubEntryCategory,
        alreadyExistingCallback: () -> Unit,
    ) = viewModelScope.launch {
        val existingFeeling = repository.feelingWithStringAndCategory(newName, subEntryCategory)
        if (existingFeeling == null) {
            repository.updateName(subEntryID = subEntryID, newName = newName)
        } else {
            alreadyExistingCallback()
        }
    }

    fun updateCheckedSubEntries(subEntryID: Pair<Long, Boolean>) {
        if (subEntryID in checkedSubEntryIDs) {
            checkedSubEntryIDs.remove(subEntryID)
        } else {
            checkedSubEntryIDs.add(subEntryID)
        }
    }
}
