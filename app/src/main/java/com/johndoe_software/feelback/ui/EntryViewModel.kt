package com.johndoe_software.feelback.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.johndoe_software.feelback.data.Entry
import com.johndoe_software.feelback.data.EntryRepository
import com.johndoe_software.feelback.data.EntryWithSubEntriesAndFeelings
import com.johndoe_software.feelback.data.FeelingWithIntensity
import com.johndoe_software.feelback.data.SubEntryRepository
import com.johndoe_software.feelback.data.SubEntryWithEntryID
import com.johndoe_software.feelback.data.daos.AppDatabase
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.sql.Timestamp

class EntryViewModel(application: Application) : AndroidViewModel(application) {
    private val entryRepository: EntryRepository
    private val subEntryRepository: SubEntryRepository
    val allEntries: LiveData<List<EntryWithSubEntriesAndFeelings>>
    private var filter = MutableLiveData<List<EntryWithSubEntriesAndFeelings>>()
    val allEntriesFiltered: LiveData<List<EntryWithSubEntriesAndFeelings>>
        get() = filter

    init {
        val entriesDao = AppDatabase.getDatabase(application).entryDao()
        entryRepository =
            EntryRepository(entriesDao)
        allEntries = entryRepository.allEntries

        val subEntryDao = AppDatabase.getDatabase(application).subEntryDao()
        subEntryRepository =
            SubEntryRepository(subEntryDao, IO)
    }

    fun insert(
        feelingsWithIntensities: List<FeelingWithIntensity>,
        note: String,
    ) {
        viewModelScope.launch {
            val entry =
                Entry(
                    0,
                    Timestamp(System.currentTimeMillis()),
                    note,
                )
            val entryID = entryRepository.insert(entry)
            val subEntries =
                feelingsWithIntensities.filter {
                    it.intensity > 0
                }.map {
                    SubEntryWithEntryID(
                        subEntryID = 0,
                        entryID = entryID,
                        feelingID = it.feelingID,
                        intensity = it.intensity,
                        isDesired = it.isDesired,
                    )
                }
            subEntryRepository.insert(subEntries)
        }
    }

    private suspend fun getFilteredEntries(minTimeStamp: Timestamp): List<EntryWithSubEntriesAndFeelings> {
        return withContext(IO) {
            entryRepository.getFilteredEntries(minTimeStamp)
        }
    }

    fun setFilterQuery(minTimeStamp: Timestamp) {
        viewModelScope.launch {
            filter.value = getFilteredEntries(minTimeStamp)
        }
    }
}
