package com.johndoe_software.feelback.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Edit
import androidx.compose.material.icons.rounded.Visibility
import androidx.compose.material.icons.rounded.VisibilityOff
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import com.johndoe_software.feelback.R
import com.johndoe_software.feelback.data.SubEntry
import com.johndoe_software.feelback.data.SubEntryCategory
import com.johndoe_software.feelback.ui.commonCompose.FeelBackChip
import com.johndoe_software.feelback.ui.commonCompose.SubEntryNameFromDialog
import com.johndoe_software.feelback.ui.statistics.LazyCardColumn

@Composable
fun VisibilityIndicator(
    visible: Boolean,
    onRemoveClicked: () -> Unit,
) {
    val icon =
        if (visible) {
            Icons.Rounded.Visibility
        } else {
            Icons.Rounded.VisibilityOff
        }
    IconButton(
        onClick = {
            onRemoveClicked()
        },
    ) {
        Icon(
            icon,
            contentDescription =
                stringResource(id = R.string.remove_button_description),
        )
    }
}

@Composable
fun CustomizationScreen(
    subEntries: List<SubEntry>,
    setVisibilityFn: (Long, Boolean) -> Unit,
    setSubEntryNameFn: (Long, String, SubEntryCategory) -> Unit,
) {
    val sortedSubEntries = subEntries.sortedBy { it.name }

    LazyCardColumn(sortedSubEntries, Modifier) { subEntries_, ix ->
        val subEntry = subEntries_[ix]
        Card {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier =
                    Modifier.fillMaxWidth()
                        .padding(start = dimensionResource(R.dimen.padding_small)),
            ) {
                FeelBackChip(
                    name = subEntry.name,
                    subEntryID = subEntry.subEntryID,
                    onClick = {},
                    selected = false,
                )

                Box(Modifier.weight(1f))

                var editSubEntryNameVisible by rememberSaveable { mutableStateOf(false) }
                if (editSubEntryNameVisible) {
                    SubEntryNameFromDialog(
                        title = stringResource(id = R.string.edit_sub_entry_name),
                        subEntry.name,
                        onConfirmFn = {
                            setSubEntryNameFn(subEntry.subEntryID, it, subEntry.category)
                            editSubEntryNameVisible = false
                        },
                        onDismissFn = { editSubEntryNameVisible = false },
                    )
                }

                IconButton(
                    onClick = {
                        editSubEntryNameVisible = true
                    },
                ) {
                    Icon(
                        Icons.Rounded.Edit,
                        contentDescription = stringResource(id = R.string.edit_button_description),
                    )
                }

                if (subEntry.subEntryID == 1L || subEntry.subEntryID == 2L) {
                    IconButton(onClick = {}, enabled = false) {
                        Icon(
                            Icons.Rounded.Visibility,
                            contentDescription = stringResource(id = R.string.remove_button_description),
                        )
                    }
                } else {
                    VisibilityIndicator(
                        visible = subEntry.visible,
                        onRemoveClicked = {
                            setVisibilityFn(subEntry.subEntryID, !subEntry.visible)
                        },
                    )
                }
            }
        }
    }
}
