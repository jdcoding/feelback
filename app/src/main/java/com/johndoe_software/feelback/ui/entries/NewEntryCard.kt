package com.johndoe_software.feelback.ui.entries

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.johndoe_software.feelback.R
import com.johndoe_software.feelback.data.FeelingWithEntriesAndSubEntries
import com.johndoe_software.feelback.data.FeelingWithIntensity
import com.johndoe_software.feelback.data.SubEntry
import com.johndoe_software.feelback.data.SubEntryCategory
import com.johndoe_software.feelback.ui.SubEntryViewModel
import com.johndoe_software.feelback.ui.commonCompose.AppTheme
import com.johndoe_software.feelback.ui.commonCompose.FeelBackChip
import com.johndoe_software.feelback.ui.commonCompose.accuracyPercent
import com.johndoe_software.feelback.ui.commonCompose.labelFromIntensity
import java.util.Locale

@Composable
fun FeelBackButton(
    onClick: () -> Unit,
    text: String,
    horizontalPadding: Dp,
) {
    Button(
        onClick = onClick,
        modifier =
            Modifier
                .fillMaxWidth()
                .padding(
                    vertical = dimensionResource(id = R.dimen.padding_small),
                    horizontal = horizontalPadding,
                ),
    ) {
        Text(
            text.uppercase(Locale.ROOT),
            color = MaterialTheme.colorScheme.onPrimary,
            style = MaterialTheme.typography.labelMedium,
        )
    }
}

data class NewEntryChipVarsDependingOnCategory(
    val category: SubEntryCategory,
    val subEntries: List<SubEntry>,
    val titleResource: Int,
    val newSubEntryDialogTitle: Int,
    val isDesired: Boolean,
)

private fun getChipGroupVars(subEntries: List<SubEntry>): MutableList<NewEntryChipVarsDependingOnCategory> {
    val categories =
        listOf(
            SubEntryCategory.ACTIVITY,
            SubEntryCategory.FEELING,
            SubEntryCategory.FEELING,
        )
    val questions =
        listOf(
            R.string.activities_question,
            R.string.feelings_question,
            R.string.desired_feelings_question,
        )

    val newSubEntryTitles =
        listOf(
            R.string.add_activity_title,
            R.string.add_feeling_title,
            R.string.add_feeling_title,
        )

    val isDesired = listOf(false, false, true)

    val subEntriesByCategory =
        categories.map { cat ->
            subEntries.filter { sub -> sub.category == cat }
        }
    val varsByCategory = mutableListOf<NewEntryChipVarsDependingOnCategory>()

    for (pos in categories.indices) {
        varsByCategory.add(
            NewEntryChipVarsDependingOnCategory(
                categories[pos],
                subEntriesByCategory[pos],
                questions[pos],
                newSubEntryTitles[pos],
                isDesired[pos],
            ),
        )
    }
    return varsByCategory
}

fun calcProbabilitiesActivitiesAreHelping(
    activitiesSubEntries: List<SubEntry>,
    checkedFeelingsIDs: List<Long>,
    feelingsWithEntriesAndSubEntries: List<FeelingWithEntriesAndSubEntries>?,
): HashMap<Long, Float> {
    val probabilityActivitiesNotHelping = HashMap<Long, Float>()
    activitiesSubEntries.forEach { activity ->
        probabilityActivitiesNotHelping[activity.subEntryID] = 1.0f
    }

    feelingsWithEntriesAndSubEntries?.let {
        checkedFeelingsIDs.map { checkedID ->
            val ix = feelingsWithEntriesAndSubEntries.indexOfFirst { it.subEntry.subEntryID == checkedID }
            val probabilityActivityHelping =
                if (ix == -1) {
                    HashMap<Long, Float>().withDefault { _ -> 1.0f }
                } else {
                    accuracyPercent(
                        feelingsWithEntriesAndSubEntries,
                        ix,
                    ).first
                }
            activitiesSubEntries.forEach { activity ->
                probabilityActivitiesNotHelping[activity.subEntryID] = (
                    1.0f - (probabilityActivityHelping[activity.subEntryID] ?: 1.0f)
                ) * (probabilityActivitiesNotHelping[activity.subEntryID] ?: 1.0f)
            }
        }
    }

    val probabilityActivitiesAreHelping = probabilityActivitiesNotHelping.map { it.key to 1.0f - it.value }.toMap()
    return probabilityActivitiesAreHelping as HashMap<Long, Float>
}

@Composable
fun GoodBadSlider(
    good: SubEntry,
    bad: SubEntry,
    sliderPosition: Float,
    onSliderPositionChange: (Float) -> Unit,
) {
    AppTheme {
        Card {
            Row(verticalAlignment = Alignment.CenterVertically) {
                FeelBackChip(name = good.name, subEntryID = good.subEntryID, onClick = {}, selected = false)
                Slider(
                    value = sliderPosition,
                    onValueChange = onSliderPositionChange,
                    valueRange = 0f..1f,
                    modifier = Modifier.weight(0.1f).padding(horizontal = dimensionResource(id = R.dimen.padding_small)),
                    colors =
                        SliderDefaults.colors(
                            thumbColor = MaterialTheme.colorScheme.onPrimaryContainer,
                            activeTrackColor = MaterialTheme.colorScheme.onPrimaryContainer,
                            inactiveTrackColor = MaterialTheme.colorScheme.primary,
                        ),
                )
                FeelBackChip(name = bad.name, subEntryID = bad.subEntryID, onClick = {}, selected = false)
            }
        }
    }
}

@Composable
private fun NewEntryScreen(
    subEntries: List<SubEntry>,
    onSaveButtonClick: (List<FeelingWithIntensity>, String) -> Unit,
    onSubEntryPlusButtonClick: (String, SubEntryCategory) -> Unit,
    subEntryViewModel: SubEntryViewModel = viewModel(),
    feelingsWithEntriesAndSubEntries: List<FeelingWithEntriesAndSubEntries>?,
) {
    Column(
        modifier =
            Modifier
                .fillMaxSize()
                .padding(
                    top = dimensionResource(id = R.dimen.padding_small),
                    start = dimensionResource(id = R.dimen.fab_margin),
                    end = dimensionResource(id = R.dimen.fab_margin),
                    bottom = dimensionResource(id = R.dimen.padding_small),
                ),
    ) {
        val varsByCategory = getChipGroupVars(subEntries)
        varsByCategory.forEach { it ->
            val checked =
                subEntryViewModel.checkedSubEntryIDs.filter { checkedID ->
                    it.subEntries.any { subEntry -> subEntry.subEntryID == checkedID.first && it.isDesired == checkedID.second }
                }.map { checkedID -> checkedID.first }
            Text(
                text = stringResource(it.titleResource),
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier.padding(top = dimensionResource(id = R.dimen.padding_small)),
            )
            if (it.category == SubEntryCategory.FEELING && !it.isDesired) {
                GoodBadSlider(it.subEntries[0], it.subEntries[1], subEntryViewModel.goodIntensity.floatValue) { sliderPos ->
                    subEntryViewModel.goodIntensity.floatValue = sliderPos
                }
            }
            val categorySubEntries =
                if (it.category == SubEntryCategory.FEELING && !it.isDesired) {
                    it.subEntries.drop(2)
                } else {
                    it.subEntries
                }

            NewEntryChipGroup(
                checked,
                { ix -> subEntryViewModel.updateCheckedSubEntries(Pair(ix, it.isDesired)) },
                categorySubEntries,
                { string -> onSubEntryPlusButtonClick(string, it.category) },
                stringResource(it.newSubEntryDialogTitle),
            )
        }

        val activitiesSubEntries = subEntries.filter { it.category == SubEntryCategory.ACTIVITY }

        val feelingSubEntries = subEntries.filter { it.category == SubEntryCategory.FEELING }
        val checkedFeelingsIDs =
            subEntryViewModel.checkedSubEntryIDs.filter { checkedID ->
                feelingSubEntries.any { subEntry -> subEntry.subEntryID == checkedID.first && checkedID.second }
            }.map { checkedID -> checkedID.first }

        val probabilityActivitiesHelping =
            calcProbabilitiesActivitiesAreHelping(
                activitiesSubEntries,
                checkedFeelingsIDs,
                feelingsWithEntriesAndSubEntries,
            )

        val sortedActivities =
            activitiesSubEntries.sortedWith(
                compareByDescending { probabilityActivitiesHelping[it.subEntryID] },
            )
        val first = sortedActivities.take(5)

        EntriesScreenChipGroup(
            title = stringResource(id = R.string.suggested_activities),
            subEntries =
                first.map {
                    Pair(
                        labelFromIntensity(it.name, probabilityActivitiesHelping[it.subEntryID] ?: 1.0f),
                        it.subEntryID,
                    )
                },
        )

        var noteText by rememberSaveable(stateSaver = TextFieldValue.Saver) {
            mutableStateOf(TextFieldValue(""))
        }
        OutlinedTextField(
            label = {
                Text(
                    text = stringResource(R.string.note),
                )
            },
            value = noteText,
            onValueChange = { noteText = it },
            modifier = Modifier.fillMaxWidth().padding(vertical = dimensionResource(id = R.dimen.padding_small)),
        )

        FeelBackButton(
            onClick = {
                val feelingsWithIntensities =
                    feelingsWithIntensitiesFromSubEntriesAndState(
                        subEntryViewModel.checkedSubEntryIDs.toList(),
                        1 - subEntryViewModel.goodIntensity.floatValue,
                    )
                onSaveButtonClick(feelingsWithIntensities, noteText.text)
                subEntryViewModel.checkedSubEntryIDs.clear()
                subEntryViewModel.goodIntensity.floatValue = 0.5f
            },
            text = stringResource(id = R.string.button_save),
            horizontalPadding = 0.dp,
        )
    }
}

private fun feelingsWithIntensitiesFromSubEntriesAndState(
    state: List<Pair<Long, Boolean>>,
    goodIntensity: Float,
): List<FeelingWithIntensity> {
    return listOf(
        FeelingWithIntensity(feelingID = 1, intensity = goodIntensity, isDesired = false),
        FeelingWithIntensity(feelingID = 2, intensity = 1.0f - goodIntensity, isDesired = false),
    ) +
        state.map {
            FeelingWithIntensity(
                feelingID = it.first,
                intensity = 1.0f,
                isDesired = it.second,
            )
        }
}

@Composable
fun ExpendableNewEntryScreen(
    subEntries: List<SubEntry>,
    onSubEntryPlusButtonClick: (String, SubEntryCategory) -> Unit,
    saveEntryFn: (List<FeelingWithIntensity>, String) -> Unit,
    noFeelingsSelectedFn: () -> Unit,
    filteredFeelingsWithEntriesAndSubEntries: List<FeelingWithEntriesAndSubEntries>?,
) {
    var expandedState by rememberSaveable { mutableStateOf("new") }

    Card(colors = CardDefaults.cardColors(MaterialTheme.colorScheme.surfaceVariant)) {
        Crossfade(targetState = expandedState, label = "NewEntryCrossfade") {
            when (it) {
                "new" -> {
                    FeelBackButton(
                        onClick = { expandedState = "editing" },
                        text = stringResource(id = R.string.new_entry_header),
                        horizontalPadding = dimensionResource(id = R.dimen.fab_margin),
                    )
                }
                "editing" -> {
                    NewEntryScreen(
                        subEntries = subEntries,
                        onSaveButtonClick = { subEntries: List<FeelingWithIntensity>, noteText: String ->
                            if (subEntries.isEmpty()) {
                                noFeelingsSelectedFn()
                            } else {
                                saveEntryFn(subEntries, noteText)
                                expandedState = "new"
                            }
                        },
                        onSubEntryPlusButtonClick = onSubEntryPlusButtonClick,
                        feelingsWithEntriesAndSubEntries = filteredFeelingsWithEntriesAndSubEntries,
                    )
                }
            }
        }
    }
}
