package com.johndoe_software.feelback.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.ViewList
import androidx.compose.material.icons.rounded.Leaderboard
import androidx.compose.material.icons.rounded.Policy
import androidx.compose.material.icons.rounded.Share
import androidx.compose.material.icons.rounded.Tune
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.johndoe_software.feelback.R
import com.johndoe_software.feelback.data.EntryWithSubEntriesAndFeelings
import com.johndoe_software.feelback.data.FeelingWithIntensity
import com.johndoe_software.feelback.data.SubEntry
import com.johndoe_software.feelback.data.SubEntryCategory
import com.johndoe_software.feelback.ui.commonCompose.AppTheme
import com.johndoe_software.feelback.ui.entries.EntriesScreen
import com.johndoe_software.feelback.ui.statistics.StatisticsScreen
import kotlinx.coroutines.launch
import java.util.Locale

sealed class NavigationItem(
    var navRoute: String,
    var navTitle: Int,
    var icon: @Composable () -> ImageVector,
) {
    object Entries : NavigationItem(
        "entries",
        R.string.entries,
        { Icons.AutoMirrored.Rounded.ViewList },
    )

    object Statistics : NavigationItem(
        "statistics",
        R.string.statistics,
        { Icons.Rounded.Leaderboard },
    )

    object Customization : NavigationItem(
        "customization",
        R.string.customization,
        { Icons.Rounded.Tune },
    )
}

@Composable
fun NavigationContent(
    navigationController: NavHostController,
    entryViewModel: EntryViewModel,
    subEntries: List<SubEntry>,
    saveEntriesFn: (List<FeelingWithIntensity>, String) -> Unit,
    noFeelingsSelectedFn: () -> Unit,
    addSubEntryFn: (String, SubEntryCategory) -> Unit,
    toggleVisibilityFn: (Long, Boolean) -> Unit,
    editSubEntryNameFn: (Long, String, SubEntryCategory) -> Unit,
) {
    NavHost(
        navController = navigationController,
        startDestination = NavigationItem.Entries.navRoute,
    ) {
        composable(NavigationItem.Entries.navRoute) {
            EntriesScreen(
                entryViewModel,
                saveEntriesFn,
                noFeelingsSelectedFn,
                addSubEntryFn,
                subEntries,
            )
        }
        composable(NavigationItem.Statistics.navRoute) {
            StatisticsScreen()
        }
        composable(NavigationItem.Customization.navRoute) {
            CustomizationScreen(
                subEntries,
                toggleVisibilityFn,
            ) { id, name, category ->
                editSubEntryNameFn(id, name, category)
            }
        }
    }
}

fun capitalize(string: String): String {
    return string.replaceFirstChar {
        if (it.isLowerCase()) {
            it.titlecase(
                Locale.ROOT,
            )
        } else {
            it.toString()
        }
    }
}

@Composable
fun BottomNavigationLayout(navigationController: NavController) {
    val bottomNavItems =
        listOf(
            NavigationItem.Entries,
            NavigationItem.Statistics,
            NavigationItem.Customization,
        )
    NavigationBar(
        containerColor = MaterialTheme.colorScheme.surface,
    ) {
        val backStackEntry by navigationController.currentBackStackEntryAsState()
        val currentNavRoute = backStackEntry?.destination?.route
        bottomNavItems.forEach { navItem ->
            NavigationBarItem(
                icon = {
                    Icon(
                        navItem.icon(),
                        contentDescription = null,
                        modifier = Modifier.width(24.dp),
                    )
                },
                label = { Text(text = capitalize(stringResource(navItem.navTitle))) },
                selected = currentNavRoute == navItem.navRoute,
                alwaysShowLabel = true,
                onClick = {
                    navigationController.navigate(navItem.navRoute) {
                        navigationController.graph.startDestinationRoute?.let { route ->
                            popUpTo(route) {
                                saveState = true
                            }
                        }
                        launchSingleTop = true
                        restoreState = true
                    }
                },
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainActivityLayout(exportFunction: (List<EntryWithSubEntriesAndFeelings>) -> Unit) {
    val navigationController = rememberNavController()
    val entryViewModel: EntryViewModel = viewModel()
    val entries = entryViewModel.allEntries.observeAsState().value
    val subEntryViewModel: SubEntryViewModel = viewModel()
    val subEntries = subEntryViewModel.allSubEntries.observeAsState().value
    AppTheme {
        val snackBarHostState = remember { SnackbarHostState() }
        val scope = rememberCoroutineScope()

        Scaffold(
            snackbarHost = {
                SnackbarHost(
                    hostState = snackBarHostState,
                )
            },
            topBar = {
                TopBar { entries?.let { exportFunction(it) } }
            },
            bottomBar = {
                BottomNavigationLayout(navigationController)
            },
            content = {
                Box(modifier = Modifier.padding(it)) {
                    val noFeelingsSelectedString = stringResource(R.string.no_subentries_selected)
                    val subEntryAlreadyExists = stringResource(R.string.insert_existing_feeling)
                    entries?.let { _ ->
                        subEntries?.let { someSubEntries ->
                            NavigationContent(
                                navigationController,
                                entryViewModel,
                                someSubEntries,
                                { feelings, note -> entryViewModel.insert(feelings, note) },
                                {
                                    scope.launch {
                                        snackBarHostState.showSnackbar(noFeelingsSelectedString)
                                    }
                                },
                                { name, category ->
                                    subEntryViewModel.addFeelingToListIfNotExisting(name, category) {
                                        scope.launch {
                                            snackBarHostState.showSnackbar(subEntryAlreadyExists)
                                        }
                                    }
                                },
                                { sub, visible ->
                                    subEntryViewModel.updateVisible(sub, visible)
                                },
                                { sub, name, category ->
                                    subEntryViewModel.updateNameIfNotExisting(sub, name, category) {
                                        scope.launch {
                                            snackBarHostState.showSnackbar(subEntryAlreadyExists)
                                        }
                                    }
                                },
                            )
                        }
                    }
                }
            },
        )
    }
}

@Composable
fun PrivacyDialog(hideDialog: () -> Unit) {
    AlertDialog(
        onDismissRequest = hideDialog,
        confirmButton = {
            TextButton(
                modifier =
                    Modifier
                        .padding(all = 8.dp)
                        .fillMaxWidth(),
                onClick = hideDialog,
            ) {
                Text(stringResource(R.string.privacy_agree_button))
            }
        },
        title = {
            Text(stringResource(R.string.privacy_dialog_title))
        },
        text = {
            Text(stringResource(R.string.privacy_dialog_text))
        },
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar(exportFunction: () -> Unit) {
    val privacyDialogVisible = rememberSaveable { mutableStateOf(true) }
    TopAppBar(
        title = { Text("FeelBack") },
        colors =
            TopAppBarDefaults.topAppBarColors(
                containerColor =
                    MaterialTheme.colorScheme.surface,
            ),
        actions = {
            IconButton(onClick = { exportFunction() }) {
                Icon(Icons.Rounded.Share, stringResource(R.string.shareIcon))
            }
            IconButton(onClick = {
                privacyDialogVisible.value = true
            }) {
                Icon(Icons.Rounded.Policy, stringResource(R.string.privacyPolicyIcon))
            }
        },
    )
    AnimatedVisibility(visible = privacyDialogVisible.value) {
        PrivacyDialog { privacyDialogVisible.value = false }
    }
}
