package com.johndoe_software.feelback.ui.commonCompose

import com.johndoe_software.feelback.data.EntryWithSubEntriesAndFeelings
import com.johndoe_software.feelback.data.FeelingWithEntriesAndSubEntries
import com.johndoe_software.feelback.ui.EntryViewModel
import com.johndoe_software.feelback.ui.statistics.intervalToDays
import com.johndoe_software.feelback.ui.statistics.milliSecondsPerDay
import com.johndoe_software.feelback.ui.statistics.todayMidnight
import java.sql.Timestamp

enum class PossibleIntervals {
    MONTH,
    QUARTER,
    YEAR,
}

fun setInterval(
    interval: PossibleIntervals,
    entryViewModel: EntryViewModel,
) {
    val days = intervalToDays[interval]!!
    val minTimeStampMilliseconds = todayMidnight() - (days - 1).toLong() * milliSecondsPerDay
    entryViewModel.setFilterQuery(Timestamp(minTimeStampMilliseconds))
}

fun entriesToSubEntries(entries: List<EntryWithSubEntriesAndFeelings>): List<FeelingWithEntriesAndSubEntries> {
    val entriesOnly = entries.map { it.entry }
    val links = entries.map { it.subEntryWithEntryIDs }.flatten()
    val subEntriesOnly = entries.map { it.subEntries }.flatten().distinct()

    val subEntries =
        subEntriesOnly.map { subEntry ->
            val relevantLinks = links.filter { it.feelingID == subEntry.subEntryID && !it.isDesired }
            val relevantEntries =
                entriesOnly.filter { entry -> entry.entryID in relevantLinks.map { it.entryID } }
            FeelingWithEntriesAndSubEntries(subEntry, relevantEntries, relevantLinks)
        }
    return subEntries
}

fun accuracyPercent(
    subEntries: List<FeelingWithEntriesAndSubEntries>,
    pivotIx: Int,
): Pair<HashMap<Long, Float>, List<FeelingWithEntriesAndSubEntries>> {
    val accuracyPercent = HashMap<Long, Float>()
    val otherSubEntries =
        subEntries.filterIndexed { matchIx, _ ->
            pivotIx != matchIx
        }
    otherSubEntries.forEach { otherSubEntry ->
        val otherEntryMass = otherSubEntry.subEntryWithEntryIDs.map { it.intensity }.sum()

        val otherEntriesIDs = otherSubEntry.entries.map { it.entryID }
        val bothEntriesIDs = subEntries[pivotIx].entries.filter { it.entryID in otherEntriesIDs }.map { it.entryID }
        val pivotMassesBoth = subEntries[pivotIx].subEntryWithEntryIDs.filter { it.entryID in bothEntriesIDs }.map { it.intensity }
        val otherMassesBoth = otherSubEntry.subEntryWithEntryIDs.filter { it.entryID in bothEntriesIDs }.map { it.intensity }
        var togetherMass = 0f
        for (i in pivotMassesBoth.indices) {
            val pivotMassBoth = pivotMassesBoth[i]
            val otherMassBoth = otherMassesBoth[i]
            togetherMass += pivotMassBoth * otherMassBoth
        }

        val probability = if (otherEntryMass == 0f) 1f else togetherMass / otherEntryMass
        accuracyPercent[otherSubEntry.subEntry.subEntryID] = probability
    }

    val sortedSubEntries =
        otherSubEntries.sortedWith(
            compareByDescending<FeelingWithEntriesAndSubEntries> {
                accuracyPercent[it.subEntry.subEntryID]
            }.thenBy {
                it.subEntry.name
            },
        )
    return Pair(accuracyPercent, sortedSubEntries)
}
