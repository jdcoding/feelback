package com.johndoe_software.feelback.ui.statistics

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.FastOutLinearInEasing
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideIn
import androidx.compose.animation.slideOut
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.johndoe_software.feelback.R
import com.johndoe_software.feelback.data.EntryWithSubEntriesAndFeelings
import com.johndoe_software.feelback.data.FeelingWithEntriesAndSubEntries
import com.johndoe_software.feelback.data.SubEntry
import com.johndoe_software.feelback.ui.commonCompose.AppTheme
import com.johndoe_software.feelback.ui.commonCompose.SubEntryChipGroup
import com.johndoe_software.feelback.ui.commonCompose.accuracyPercent
import com.johndoe_software.feelback.ui.commonCompose.colorFromSubEntryID
import com.johndoe_software.feelback.ui.commonCompose.getPalettes
import com.johndoe_software.feelback.ui.commonCompose.labelFromIntensity
import com.patrykandpatrick.vico.compose.cartesian.CartesianChartHost
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberAxisGuidelineComponent
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberAxisLabelComponent
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberBottomAxis
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberStartAxis
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLineCartesianLayer
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLineSpec
import com.patrykandpatrick.vico.compose.cartesian.rememberCartesianChart
import com.patrykandpatrick.vico.compose.cartesian.rememberVicoScrollState
import com.patrykandpatrick.vico.compose.common.shader.color
import com.patrykandpatrick.vico.core.cartesian.HorizontalLayout
import com.patrykandpatrick.vico.core.cartesian.axis.AxisItemPlacer
import com.patrykandpatrick.vico.core.cartesian.axis.BaseAxis
import com.patrykandpatrick.vico.core.cartesian.data.AxisValueOverrider
import com.patrykandpatrick.vico.core.cartesian.data.CartesianChartModel
import com.patrykandpatrick.vico.core.cartesian.data.LineCartesianLayerModel.Companion.build
import com.patrykandpatrick.vico.core.common.shader.DynamicShader
import com.patrykandpatrick.vico.core.common.shape.Shape
import java.time.DayOfWeek
import java.util.Calendar
import kotlin.math.sqrt

@Suppress("ktlint:standard:function-naming")
@Composable
fun FeelBackDivider() {
    HorizontalDivider(
        color = MaterialTheme.colorScheme.onSurfaceVariant.copy(alpha = 0.4f),
        modifier = Modifier.padding(vertical = dimensionResource(R.dimen.fab_margin)),
    )
}

@Composable
fun getNumberToLabelFn(days: Int): (Float) -> String {
    val todayString = stringResource(R.string.today)
    val weekString = stringResource(R.string.a_week_ago)
    val twoWeeksString = stringResource(R.string.two_weeks_ago)
    val threeWeeksString = stringResource(R.string.three_weeks_ago)
    val fourWeeksString = stringResource(R.string.four_weeks_ago)
    val monthString = stringResource(R.string.a_month_ago)
    val twoMonthsString = stringResource(R.string.two_months_ago)
    val threeMonthsString = stringResource(R.string.three_months_ago)
    val sixMonthsString = stringResource(R.string.six_months_ago)
    val nineMonthsString = stringResource(R.string.nine_months_ago)
    val yearString = stringResource(R.string.a_year_ago)
    when (days) {
        30 -> {
            return {
                when (it.toInt()) {
                    0 -> todayString
                    -7 -> weekString
                    -14 -> twoWeeksString
                    -21 -> threeWeeksString
                    -28 -> fourWeeksString
                    else -> ""
                }
            }
        }
        91 -> {
            return {
                when (it.toInt()) {
                    0 -> todayString
                    -30 -> monthString
                    -60 -> twoMonthsString
                    -90 -> threeMonthsString
                    else -> ""
                }
            }
        }
        else -> {
            return {
                when (it.toInt()) {
                    0 -> todayString
                    -90 -> threeMonthsString
                    -180 -> sixMonthsString
                    -270 -> nineMonthsString
                    -360 -> yearString
                    else -> ""
                }
            }
        }
    }
}

@Suppress("ktlint:standard:function-naming")
@Composable
fun CorrelationSection(
    subEntries: List<FeelingWithEntriesAndSubEntries>,
    ix: Int,
) {
    Text(
        stringResource(R.string.correlation_title),
        style = MaterialTheme.typography.titleMedium,
        modifier = Modifier.padding(bottom = dimensionResource(R.dimen.padding_small)),
    )

    val percentAndSortedSubEntries = accuracyPercent(subEntries, ix)
    val accuracyPercent = percentAndSortedSubEntries.first
    val sortedSubEntries = percentAndSortedSubEntries.second

    val pairs =
        sortedSubEntries.map {
            Pair(
                labelFromIntensity(it.subEntry.name, accuracyPercent[it.subEntry.subEntryID] ?: 0f),
                it.subEntry.subEntryID,
            )
        }
    SubEntryChipGroup(pairs)
}

@Suppress("ktlint:standard:function-naming")
@Composable
fun ExtendableStatisticsCard(
    visible: Boolean,
    entries: List<EntryWithSubEntriesAndFeelings>,
    subEntries: List<FeelingWithEntriesAndSubEntries>,
    ix: Int,
    days: Int,
) {
    AnimatedVisibility(
        visible,
        enter =
            slideIn(tween(100, easing = FastOutLinearInEasing)) { fullSize ->
                IntOffset(0, -fullSize.height / 4)
            },
        exit =
            slideOut(tween(100, easing = FastOutLinearInEasing)) { fullSize ->
                IntOffset(0, -fullSize.height / 4)
            },
    ) {
        Column(
            modifier =
                Modifier.padding(
                    start = dimensionResource(R.dimen.padding_small),
                    end = dimensionResource(R.dimen.padding_small),
                    bottom = dimensionResource(R.dimen.padding_small),
                ),
        ) {
            val (backgroundPalette, _) = getPalettes()
            val subEntryID = subEntries[ix].subEntry.subEntryID
            val color = colorFromSubEntryID(subEntryID, backgroundPalette)
            val dayStats = calcDayStats(entries, subEntryID, days)
            DayChart(dayStats, color, days)
            val hourlyStats = calcHourlyStats(entries, subEntries[ix].subEntry)
            HourlyChart(hourlyStats, color)
            val weekDayStats = calcWeekDayStats(entries, subEntries[ix].subEntry)
            WeekDayChart(weekDayStats, color)
            FeelBackDivider()
            CorrelationSection(subEntries, ix)
        }
    }
}

data class Stats(
    val timeUnit: Int,
    val average: Float,
    val standardDeviation: Float,
)

fun calcStats(
    entries: List<EntryWithSubEntriesAndFeelings>,
    subEntryID: Long,
    interval: IntRange,
    timeUnitExtractor: (Long) -> Int,
): List<Stats> {
    val statsMap = mutableMapOf<Int, MutableList<Float>>()

    entries.forEach { entry ->
        val timeUnit = timeUnitExtractor(entry.entry.timeStamp.time)
        if (subEntryID in entry.subEntryWithEntryIDs.filter { !it.isDesired }.map { it.feelingID }) {
            statsMap.getOrPut(timeUnit) { mutableListOf() }.add(
                entry.subEntryWithEntryIDs.first { !it.isDesired && it.feelingID == subEntryID }.intensity,
            )
        } else {
            statsMap.getOrPut(timeUnit) { mutableListOf() }.add(0f)
        }
    }

    val initValue = 1f
    val stats =
        interval.map { timeUnit ->
            val (alpha, beta) =
                statsMap[timeUnit]?.let { intensities ->
                    val probMassPos = intensities.sum()
                    val probMassNeg = intensities.size - probMassPos
                    Pair(initValue + probMassPos, initValue + probMassNeg)
                } ?: Pair(initValue, initValue)

            val mean = alpha / (alpha + beta)
            val variance = alpha * beta / ((alpha + beta) * (alpha + beta) * (alpha + beta + 1))
            val stdDev = sqrt(variance)
            Stats(timeUnit, mean, stdDev)
        }
    return stats
}

fun calcHourlyStats(
    entries: List<EntryWithSubEntriesAndFeelings>,
    subEntry: SubEntry,
): List<Stats> {
    val hourlyStats =
        calcStats(entries, subEntry.subEntryID, (0..23)) { timeStamp ->
            val calendar = Calendar.getInstance().apply { timeInMillis = timeStamp }
            calendar.get(Calendar.HOUR_OF_DAY)
        }
    val stats24 = Stats(24, hourlyStats[0].average, hourlyStats[0].standardDeviation)
    return hourlyStats + listOf(stats24)
}

data class VicoChartParams(
    val numberToLabelFn: (Float) -> String,
    val minX: Float,
    val maxX: Float,
    val spacing: Int,
    val offset: Int,
)

fun vicoChartParamsDay(): VicoChartParams {
    val numberToLabelFn = { it: Float ->

        when (it.toInt()) {
            0 -> "0:00"
            6 -> "6:00"
            12 -> "12:00"
            18 -> "18:00"
            24 -> "24:00"
            else -> ""
        }
    }
    return VicoChartParams(numberToLabelFn, 0f, 23f, 6, 0)
}

fun modelFromStats(stats: List<Stats>): CartesianChartModel {
    val x = stats.map { it.timeUnit.toFloat() }
    val y = stats.map { it.average }
    val std = stats.map { it.standardDeviation }
    val upper = (y zip std).map { it.first + it.second }
    val lower = (y zip std).map { it.first - it.second }

    return CartesianChartModel(
        build {
            series(x, upper)
            series(x, y)
            series(x, lower)
        },
    )
}

@Suppress("ktlint:standard:function-naming")
@Composable
fun HourlyChart(
    hourlyStats: List<Stats>,
    color: Color,
) {
    val model = modelFromStats(hourlyStats)
    Text(
        stringResource(R.string.hourly_plot_title),
        style = MaterialTheme.typography.titleMedium,
        modifier = Modifier.padding(bottom = dimensionResource(R.dimen.padding_small)),
    )

    VicoChart(model, color, vicoChartParamsDay())
}

fun calcWeekDayStats(
    entries: List<EntryWithSubEntriesAndFeelings>,
    subEntry: SubEntry,
): List<Stats> {
    val weekDayStats =
        calcStats(entries, subEntry.subEntryID, (1..7)) { timeStamp ->
            val calendar = Calendar.getInstance().apply { timeInMillis = timeStamp }
            calendar.get(Calendar.DAY_OF_WEEK)
        }
    val statsSunday = Stats(8, weekDayStats[0].average, weekDayStats[0].standardDeviation)
    val statsMonday = Stats(9, weekDayStats[1].average, weekDayStats[1].standardDeviation)
    return weekDayStats + listOf(statsSunday, statsMonday)
}

@Composable
fun vicoChartParamsWeekDay(): VicoChartParams {
    val weekDayStrings = DayOfWeek.values().map { it.toString().lowercase().replaceFirstChar { it.uppercase() } }
    val numberToLabelFn = { it: Float ->
        when (it.toInt()) {
            2 -> weekDayStrings[0]
            3 -> weekDayStrings[1]
            4 -> weekDayStrings[2]
            5 -> weekDayStrings[3]
            6 -> weekDayStrings[4]
            7 -> weekDayStrings[5]
            8 -> weekDayStrings[6]
            else -> ""
        }
    }
    return VicoChartParams(numberToLabelFn, 2f, 8f, 1, 0)
}

@Suppress("ktlint:standard:function-naming")
@Composable
fun WeekDayChart(
    weekDayStats: List<Stats>,
    color: Color,
) {
    val model = modelFromStats(weekDayStats)
    Text(
        stringResource(R.string.weekday_plot_title),
        style = MaterialTheme.typography.titleMedium,
        modifier = Modifier.padding(bottom = dimensionResource(R.dimen.padding_small)),
    )
    VicoChart(model, color, vicoChartParamsWeekDay())
}

@Composable
fun vicoChartParamsInterval(days: Int): VicoChartParams {
    val numberToLabelFn = getNumberToLabelFn(days)
    val (xlim, spacing, offset) =
        when (days) {
            30 -> Triple(30f, 7, 2)
            91 -> Triple(91f, 30, 1)
            else -> Triple(365f, 90, 5)
        }
    return VicoChartParams(numberToLabelFn, -xlim, 0f, spacing, offset)
}

fun calcDayStats(
    entries: List<EntryWithSubEntriesAndFeelings>,
    subEntryID: Long,
    days: Int,
): List<Stats> {
    val today = Calendar.getInstance()
    val dayStats =
        calcStats(entries, subEntryID, (-days..0)) { timeStamp ->
            val calendar = Calendar.getInstance().apply { timeInMillis = timeStamp }
            val diff = today.get(Calendar.DAY_OF_YEAR) - calendar.get(Calendar.DAY_OF_YEAR)
            -diff
        }
    return dayStats
}

@Suppress("ktlint:standard:function-naming")
@Composable
fun DayChart(
    dayStats: List<Stats>,
    color: Color,
    days: Int,
) {
    Text(
        stringResource(R.string.plot_title),
        style = MaterialTheme.typography.titleMedium,
        modifier = Modifier.padding(bottom = dimensionResource(R.dimen.padding_small)),
    )
    val model = modelFromStats(dayStats)
    val params = vicoChartParamsInterval(days)
    VicoChart(model, color, params)
}

@Suppress("ktlint:standard:function-naming")
@Composable
fun VicoChart(
    model: CartesianChartModel,
    color: Color,
    params: VicoChartParams,
) {
    CartesianChartHost(
        modifier = Modifier.padding(8.dp),
        chart =
            rememberCartesianChart(
                rememberLineCartesianLayer(
                    listOf(
                        rememberLineSpec(
                            shader = DynamicShader.color(color.copy(alpha = 0.5f)),
                            backgroundShader =
                                DynamicShader.color(color.copy(alpha = 0.3f)),
                        ),
                        rememberLineSpec(
                            shader = DynamicShader.color(color),
                            backgroundShader =
                                DynamicShader.color(color.copy(alpha = 0.0f)),
                        ),
                        rememberLineSpec(
                            shader = DynamicShader.color(color.copy(alpha = 0.5f)),
                            backgroundShader =
                                DynamicShader.color(MaterialTheme.colorScheme.surfaceVariant),
                        ),
                    ),
                    axisValueOverrider =
                        AxisValueOverrider.fixed(
                            minY = 0f,
                            maxY = 1f,
                            minX = params.minX,
                            maxX = params.maxX,
                        ),
                ),
                startAxis =
                    rememberStartAxis(
                        label = null,
                        axis = null,
                        itemPlacer =
                            remember {
                                AxisItemPlacer.Vertical.count({ _ -> 2 })
                            },
                        guideline =
                            rememberAxisGuidelineComponent(
                                shape = Shape.Rectangle,
                            ),
                    ),
                bottomAxis =
                    rememberBottomAxis(
                        guideline = null,
                        valueFormatter = { it, _, _ ->
                            params.numberToLabelFn(it)
                        },
                        itemPlacer =
                            AxisItemPlacer.Horizontal.default(
                                spacing = params.spacing,
                                offset = params.offset,
                                shiftExtremeTicks = true,
                            ),
                        label =
                            rememberAxisLabelComponent(
                                typeface = android.graphics.Typeface.DEFAULT,
                                textSize = 8.sp,
                            ),
                        labelRotationDegrees = -90f,
                        sizeConstraint = BaseAxis.SizeConstraint.TextWidth("four months ago..."),
                    ),
            ),
        model = model,
        horizontalLayout = HorizontalLayout.FullWidth(unscalableStartPaddingDp = 8f, unscalableEndPaddingDp = 8f),
        scrollState = rememberVicoScrollState(scrollEnabled = false),
    )
}

@Suppress("ktlint:standard:function-naming")
@Preview("Line Chart Dark", widthDp = 300)
@Composable
fun ExampleVicoChart() {
    val model =
        CartesianChartModel(
            build {
                series(
                    x = listOf(-30f, -14f, -8f, -7f, -6f, -2f, -1f, 0f),
                    y = listOf(0.5, 0.4, 0.5, 0.7, 0.4, 1.0, 0.9, 0.6),
                )
                series(
                    x = listOf(-30f, -14f, -8f, -7f, -6f, -2f, -1f, 0f),
                    y = listOf(0.3, 0.2, 0.3, 0.5, 0.2, 0.8, 0.7, 0.4),
                )
                series(
                    x = listOf(-30f, -14f, -8f, -7f, -6f, -2f, -1f, 0f),
                    y = listOf(0.1, 0.0, 0.1, 0.3, 0.0, 0.6, 0.5, 0.2),
                )
            },
        )
    AppTheme {
        Card {
            val color = getPalettes().first[4]
            val params = vicoChartParamsInterval(30)
            VicoChart(model, color, params)
        }
    }
}
