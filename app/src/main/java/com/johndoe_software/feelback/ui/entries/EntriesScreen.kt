package com.johndoe_software.feelback.ui.entries

import android.text.format.DateUtils
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import com.johndoe_software.feelback.R
import com.johndoe_software.feelback.data.EntryWithSubEntriesAndFeelings
import com.johndoe_software.feelback.data.FeelingWithIntensity
import com.johndoe_software.feelback.data.SubEntry
import com.johndoe_software.feelback.data.SubEntryCategory
import com.johndoe_software.feelback.ui.EntryViewModel
import com.johndoe_software.feelback.ui.commonCompose.PossibleIntervals
import com.johndoe_software.feelback.ui.commonCompose.entriesToSubEntries
import com.johndoe_software.feelback.ui.commonCompose.setInterval
import kotlinx.coroutines.launch

@Composable
fun EntriesScreen(
    entryViewModel: EntryViewModel,
    saveEntriesFn: (List<FeelingWithIntensity>, String) -> Unit,
    noFeelingsSelectedFn: () -> Unit,
    addSubEntryFn: (String, SubEntryCategory) -> Unit,
    subEntries: List<SubEntry>,
) {
    val scrollState = rememberLazyListState()
    val coroutineScope = rememberCoroutineScope()

    setInterval(PossibleIntervals.MONTH, entryViewModel)
    val entries = entryViewModel.allEntriesFiltered.observeAsState().value
    val filteredFeelingsWithEntriesAndSubEntries = entries?.let { entriesToSubEntries(it) }?.filter { it.subEntry.visible }
    LazyColumn(
        state = scrollState,
        verticalArrangement =
            Arrangement.spacedBy(
                dimensionResource(R.dimen.padding_small),
            ),
        contentPadding = PaddingValues(dimensionResource(id = R.dimen.padding_small)),
    ) {
        entries?.let { it ->
            items(it.size + 1) { index ->
                if (0 == index) {
                    ExpendableNewEntryScreen(
                        subEntries.filter { it.visible },
                        addSubEntryFn,
                        { l: List<FeelingWithIntensity>, s: String ->
                            saveEntriesFn(l, s)
                            coroutineScope.launch { scrollState.scrollToItem(0) }
                        },
                        noFeelingsSelectedFn,
                        filteredFeelingsWithEntriesAndSubEntries,
                    )
                } else {
                    EntryToUIItem(it[index - 1])
                }
            }
        }
    }
}

@Suppress("ktlint:standard:function-naming")
@Composable
fun EntryToUIItem(current: EntryWithSubEntriesAndFeelings) {
    val timeStamp =
        DateUtils.formatDateTime(
            LocalContext.current,
            current.entry.timeStamp.time,
            DateUtils.FORMAT_SHOW_TIME + DateUtils.FORMAT_SHOW_DATE +
                DateUtils.FORMAT_SHOW_YEAR + DateUtils.FORMAT_SHOW_WEEKDAY,
        )
    val nonzeroEmotions = mutableListOf<Pair<SubEntry, FeelingWithIntensity>>()
    val intensitiesAndAreDesired = current.subEntryWithEntryIDs.map { Pair(it.intensity, it.isDesired) }
    (current.subEntries zip intensitiesAndAreDesired).forEach { (feeling, intensityAndIsDesired) ->
        if (intensityAndIsDesired.first > 0) {
            nonzeroEmotions.add(
                Pair(
                    feeling,
                    FeelingWithIntensity(feeling.subEntryID, intensityAndIsDesired.first, intensityAndIsDesired.second),
                ),
            )
        }
    }
    EntriesScreenItem(timeStamp = timeStamp, subEntries = nonzeroEmotions, note = current.entry.note)
}
