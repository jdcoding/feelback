package com.johndoe_software.feelback

import androidx.room.migration.AutoMigrationSpec
import androidx.room.testing.MigrationTestHelper
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.test.platform.app.InstrumentationRegistry
import com.johndoe_software.feelback.data.daos.AppDatabase
import com.johndoe_software.feelback.data.daos.MIGRATION_1_2
import com.johndoe_software.feelback.data.daos.MIGRATION_3_4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@RunWith(MockitoJUnitRunner::class)
class MigrationTest {
    private val testDB = "migration-test"

    @Rule @JvmField
    val helper: MigrationTestHelper =
        MigrationTestHelper(
            InstrumentationRegistry.getInstrumentation(),
            AppDatabase::class.java,
            emptyList<AutoMigrationSpec>(),
        )

    @Test
    @Throws(IOException::class)
    fun migrate1To2() {
        helper.createDatabase(testDB, 1).apply {
            execSQL("INSERT INTO sub_entries VALUES (1, 1, 1, 0)")
            assert(hasItem(this, "sub_entries"))
            close()
        }

        val db =
            helper.runMigrationsAndValidate(
                testDB,
                2,
                true,
                MIGRATION_1_2,
            )

        assert(!hasItem(db, "sub_entries"))
    }

    @Test
    @Throws(IOException::class)
    fun migrate3To4() {
        helper.createDatabase(testDB, 3).apply {
            execSQL("INSERT INTO feelings VALUES (1, 'happy', 1, 'ACTIVITY', 0)")
            assert(hasItem(this, "feelings"))
            close()
        }

        val db =
            helper.runMigrationsAndValidate(
                testDB,
                4,
                true,
                MIGRATION_3_4,
            )

        assert(hasItem(db, "feelings"))
    }
}

fun hasItem(
    db: SupportSQLiteDatabase,
    table: String,
): Boolean {
    val queryString = "SELECT * from $table"
    db.apply {
        val cursor = query(queryString)
        return cursor.moveToFirst()
    }
}
