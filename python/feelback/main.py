

from datetime import datetime
from typing import Literal
import pandas
from pydantic import BaseModel


class Entry(BaseModel):
    entry_id: int
    time_stamp: datetime


class Entries(BaseModel):
    entries: list[Entry]


def read_entries(path: str) -> list[Entry]:
    entries = pandas.read_csv(path, parse_dates=["time_stamp"])
    Entries(entries=entries.to_dict(orient="records"))
    entries.set_index('entry_id')
    return entries


SubEntryCategory = Literal["FEELING", "ACTIVITY"]


class SubEntry(BaseModel):
    sub_entry_id: int
    name: str
    category: SubEntryCategory
    visible: bool


class SubEntries(BaseModel):
    sub_entries: list[SubEntry]


def read_sub_entries(path: str) -> list[SubEntry]:
    sub_entries = pandas.read_csv(path)
    SubEntries(sub_entries=sub_entries.to_dict(orient="records"))
    sub_entries = sub_entries.set_index('sub_entry_id')
    return sub_entries


class SubEntryWithEntryID(BaseModel):
    sub_entry_id: int
    entry_id: int


class SubEntryWithEntryIDs(BaseModel):
    sub_entries_with_entry_ids: list[SubEntryWithEntryID]


def read_sub_entries_with_entry_ids(path: str) -> list[SubEntryWithEntryID]:
    sub_entries_with_entry_ids = pandas.read_csv(path)
    SubEntryWithEntryIDs(
        sub_entries_with_entry_ids=sub_entries_with_entry_ids.to_dict(orient="records"))
    return sub_entries_with_entry_ids
