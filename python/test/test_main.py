import os

from feelback.main import (
    SubEntry,
    SubEntryCategory,
    SubEntryWithEntryID,
    read_entries,
    read_sub_entries,
    read_sub_entries_with_entry_ids,
)

import pandas as pd
import yaml
import numpy as np
from bokeh.palettes import Viridis256, RdBu11
from bokeh.models import ColumnDataSource, Text, Line, Range1d
from bokeh.plotting import figure, show, column, Figure
from bokeh.transform import linear_cmap


def data_path(file_name: str) -> str:
    return os.path.join(os.path.dirname(__file__), "../../data", file_name)


def test_read_entries():
    entries = read_entries(data_path("entries.csv"))
    print(entries)


def test_read_sub_entries():
    sub_entries = read_sub_entries(data_path("sub-entries.csv"))
    print(sub_entries)


def test_read_sub_entries_with_entry_ids():
    sub_entries_with_entry_ids = read_sub_entries_with_entry_ids(
        data_path("sub-entries-with-entry-ids.csv")
    )
    print(sub_entries_with_entry_ids)


interval_to_int = {"MONTH": 30, "QUARTER": 91, "YEAR": 365}


def get_percentage_intervals(
    sub_entry_string: str, sub_entries, links, entries, interval: str
):
    subentry_id = sub_entries.index[sub_entries.name == sub_entry_string][0]
    sub_entry_links = links[links.sub_entry_id == subentry_id]
    entry_ids = sub_entry_links.entry_id
    entries_by_day = entries.groupby(pd.Grouper(key="time_stamp", freq="D")).count()
    sub_entry_entries = entries[entries["entry_id"].isin(entry_ids)]
    sub_entry_entries_by_day = sub_entry_entries.groupby(
        pd.Grouper(key="time_stamp", freq="D")
    ).count()
    percentage_by_interval = sub_entry_entries_by_day / entries_by_day
    interval_days = interval_to_int[interval]
    return percentage_by_interval.tail(interval_days), entries_by_day.tail(
        interval_days
    )


def plot_percentages(percentages: pd.DataFrame, title: str) -> Figure:
    p = figure(height=300, width=1800, x_axis_type="datetime")
    bar_width_ms = 7 * 24 * 60 * 60 * 1000
    p.vbar(x=percentages.index, top=percentages.entry_id, width=bar_width_ms)
    p.vbar(
        x=percentages.index,
        top=1,
        bottom=percentages.entry_id,
        width=bar_width_ms,
        color="#eeeeee",
    )
    p.title = title
    return p


def test_plot_sub_entry_frequency_over_time():
    sub_entry_string = "müde"
    sub_entries = read_sub_entries(data_path("sub-entries.csv"))
    links = read_sub_entries_with_entry_ids(data_path("sub-entries-with-entry-ids.csv"))
    entries = read_entries(data_path("entries.csv"))
    percentages, _ = get_percentage_intervals(
        sub_entry_string, sub_entries, links, entries, interval="YEAR"
    )
    p = plot_percentages(percentages=percentages, title=sub_entry_string)
    show(p)


def plot_transparent_line(
    percentages: pd.DataFrame, counts: pd.DataFrame, title: str
) -> Figure:
    max_points_per_day = 2
    counts["alpha"] = (
        np.minimum(max_points_per_day, counts["entry_id"].values) / max_points_per_day
    )

    rolling_average_days = int(np.floor(len(counts) / 10))
    counts["alpha"] = counts["alpha"].rolling(rolling_average_days, center=True).mean()
    counts.dropna(inplace=True)
    percentages = percentages.fillna(percentages.mean())
    percentages["entry_id"] = (
        percentages["entry_id"].rolling(rolling_average_days, center=True).mean()
    )
    percentages.dropna(inplace=True)

    p = figure(height=300, width=900, x_axis_type="datetime")
    p.title = title
    p.y_range = Range1d(0, 1)
    time = counts.index.values

    t_0 = time[0]
    for t_ix in range(1, len(time)):
        t_1 = time[t_ix]
        x = [t_0, t_1]
        y = percentages.loc[t_0:t_1]["entry_id"]
        source = ColumnDataSource(dict(x=x, y=y))
        alpha = counts.loc[t_0:t_1]["alpha"].sum() / 2
        line = Line(line_color="#f46d43", line_alpha=alpha)
        p.add_glyph(source, line)
        t_0 = t_1
    return p


def test_transparent_line_plot():
    sub_entry_string = "Leichtigkeit 🎈"
    sub_entries = read_sub_entries(data_path("sub-entries.csv"))
    links = read_sub_entries_with_entry_ids(data_path("sub-entries-with-entry-ids.csv"))
    entries = read_entries(data_path("entries.csv"))
    percentages, counts = get_percentage_intervals(
        sub_entry_string, sub_entries, links, entries, interval="QUARTER"
    )
    p = plot_transparent_line(
        percentages=percentages, counts=counts, title=sub_entry_string
    )
    show(p)


def timeseries_sub_entries_from_yaml(path: str) -> list[str]:
    with open(path) as f:
        return yaml.safe_load(f)


def test_multiple_transparent_line_plots():
    sub_entry_strings = timeseries_sub_entries_from_yaml(
        os.path.join(os.path.dirname(__file__), "../timeseries-subentries.yaml")
    )
    sub_entries = read_sub_entries(data_path("sub-entries.csv"))
    links = read_sub_entries_with_entry_ids(data_path("sub-entries-with-entry-ids.csv"))
    entries = read_entries(data_path("entries.csv"))
    plots = []
    for sub_entry_string in sub_entry_strings:
        percentages, counts = get_percentage_intervals(
            sub_entry_string, sub_entries, links, entries, interval="MONTH"
        )
        plots.append(
            plot_transparent_line(
                percentages=percentages, counts=counts, title=sub_entry_string
            )
        )
    show(column(*plots))


def test_multiple_timeseries():
    sub_entry_strings = timeseries_sub_entries_from_yaml(
        os.path.join(os.path.dirname(__file__), "../timeseries-subentries.yaml")
    )
    sub_entries = read_sub_entries(data_path("sub-entries.csv"))
    links = read_sub_entries_with_entry_ids(data_path("sub-entries-with-entry-ids.csv"))
    entries = read_entries(data_path("entries.csv"))
    plots = []
    for sub_entry_string in sub_entry_strings:
        percentages, _ = get_percentage_intervals(
            sub_entry_string, sub_entries, links, entries
        )
        plots.append(plot_percentages(percentages=percentages, title=sub_entry_string))
    show(column(*plots))


def get_one_category_sub_entries_sorted(
    sub_entries: list[SubEntry],
    links: list[SubEntryWithEntryID],
    category: SubEntryCategory,
):
    category_sub_entries = sub_entries[sub_entries.category == category]
    category_sub_entries["num_entries"] = [
        links[links.sub_entry_id == id].count().sub_entry_id
        for id in category_sub_entries.index
    ]
    return category_sub_entries.sort_values(by="num_entries")


def test_feeling_numbers():
    sub_entries = read_sub_entries(data_path("sub-entries.csv"))
    links = read_sub_entries_with_entry_ids(data_path("sub-entries-with-entry-ids.csv"))
    feelings = get_one_category_sub_entries_sorted(sub_entries, links, "FEELING")
    p = figure(x_range=feelings.name, width=800, height=200)
    source = ColumnDataSource(feelings)
    colormapper = linear_cmap(
        field_name="num_entries",
        palette=Viridis256,
        low=0,
        high=max(feelings.num_entries),
    )
    p.rect(
        x="name",
        y=0,
        source=source,
        width=10,
        height=20,
        color=colormapper,
        width_units="screen",
        height_units="screen",
    )
    glyph = Text(
        x="name",
        y=0,
        text="num_entries",
        angle=np.pi / 2,
        x_offset=8,
        y_offset=10,
        text_color="#FFFFFF",
        text_font_size="12px",
    )
    p.add_glyph(source, glyph)
    p.xaxis.major_label_orientation = np.pi / 3
    show(p)


def test_activity_numbers():
    sub_entries = read_sub_entries(data_path("sub-entries.csv"))
    links = read_sub_entries_with_entry_ids(data_path("sub-entries-with-entry-ids.csv"))
    activities = get_one_category_sub_entries_sorted(sub_entries, links, "ACTIVITY")
    p = figure(y_range=activities.name, width=200, height=800)
    source = ColumnDataSource(activities)
    colormapper = linear_cmap(
        field_name="num_entries",
        palette=Viridis256,
        low=0,
        high=max(activities.num_entries),
    )
    p.rect(
        y="name",
        x=0,
        source=source,
        width=20,
        height=20,
        color=colormapper,
        width_units="screen",
        height_units="screen",
    )
    glyph = Text(
        y="name",
        x=0,
        text="num_entries",
        angle=0,
        x_offset=-8,
        y_offset=10,
        text_color="#FFFFFF",
        text_font_size="12px",
    )
    p.add_glyph(source, glyph)
    show(p)


def calc_correlations(links, sub_entries_1, sub_entries_2, column_names: list[str]):
    entry_ids = set(links.entry_id.unique())
    matrix = pd.DataFrame(columns=[column_names[0], column_names[1], "count"])
    for sub_entries_1_id in sub_entries_1.index:
        for sub_entries_2_id in sub_entries_2.index:
            sub_entries_1_entry_ids = set(
                links[links.sub_entry_id == sub_entries_1_id].entry_id
            )
            sub_entries_2_entry_ids = set(
                links[links.sub_entry_id == sub_entries_2_id].entry_id
            )
            num_links = len(
                sub_entries_2_entry_ids.intersection(sub_entries_1_entry_ids)
            )

            no_activity_entry_ids = entry_ids - set(sub_entries_2_entry_ids)
            no_feeling_entry_ids = entry_ids - set(sub_entries_1_entry_ids)

            num_links += len(no_activity_entry_ids.intersection(no_feeling_entry_ids))
            num_links = int(num_links / len(entry_ids) * 100)
            sub_entry_1 = sub_entries_1[
                sub_entries_1.index == sub_entries_1_id
            ].name.to_numpy()[0]
            sub_entry_2 = sub_entries_2[
                sub_entries_2.index == sub_entries_2_id
            ].name.to_numpy()[0]
            matrix = matrix.append(
                {
                    column_names[0]: sub_entry_1,
                    column_names[1]: sub_entry_2,
                    "count": num_links,
                },
                ignore_index=True,
            )

    return matrix


def test_correlation_matrix():
    sub_entries = read_sub_entries(data_path("sub-entries.csv"))
    links = read_sub_entries_with_entry_ids(data_path("sub-entries-with-entry-ids.csv"))
    feelings = get_one_category_sub_entries_sorted(sub_entries, links, "FEELING")
    activities = get_one_category_sub_entries_sorted(sub_entries, links, "ACTIVITY")

    matrix = calc_correlations(links, feelings, activities, ["feeling", "activity"])
    p = figure(x_range=activities.name, y_range=feelings.name, width=1200, height=800)
    source = ColumnDataSource(matrix)
    colormapper = linear_cmap(field_name="count", palette=RdBu11, low=0, high=100)
    p.rect(
        x="activity",
        y="feeling",
        source=source,
        width=20,
        height=20,
        color=colormapper,
        width_units="screen",
        height_units="screen",
    )
    glyph = Text(
        x="activity",
        y="feeling",
        text="count",
        angle=0,
        x_offset=-8,
        y_offset=10,
        text_color="#FFFFFF",
        text_font_size="12px",
    )
    p.add_glyph(source, glyph)
    p.xaxis.major_label_orientation = np.pi / 3
    show(p)


def test_all_correlation_matrices():
    sub_entries = read_sub_entries(data_path("sub-entries.csv"))
    links = read_sub_entries_with_entry_ids(data_path("sub-entries-with-entry-ids.csv"))

    categories = ["FEELING", "ACTIVITY"]
    for x in categories:
        for y in categories:
            x_sub_entries = get_one_category_sub_entries_sorted(sub_entries, links, x)
            y_sub_entries = get_one_category_sub_entries_sorted(sub_entries, links, y)
            y += "_2"

            matrix = calc_correlations(links, x_sub_entries, y_sub_entries, [x, y])
            p = figure(
                x_range=x_sub_entries.name,
                y_range=y_sub_entries.name,
                width=1200,
                height=800,
            )
            source = ColumnDataSource(matrix)
            colormapper = linear_cmap(
                field_name="count", palette=RdBu11, low=0, high=100
            )
            p.rect(
                x=x,
                y=y,
                source=source,
                width=20,
                height=20,
                color=colormapper,
                width_units="screen",
                height_units="screen",
            )
            glyph = Text(
                x=x,
                y=y,
                text="count",
                angle=0,
                x_offset=-8,
                y_offset=10,
                text_color="#FFFFFF",
                text_font_size="12px",
            )
            p.add_glyph(source, glyph)
            p.xaxis.major_label_orientation = np.pi / 3
            show(p)
