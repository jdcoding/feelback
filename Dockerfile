FROM alvrme/alpine-android:android-34-jdk17
# install OS packages
RUN apk update
RUN apk add ruby ruby-dev
# We use this for xxd hex->binary
RUN apk add vim
# install FastLane
COPY Gemfile.lock .
COPY Gemfile .
RUN gem install bundler
# json requires unf_ext requires g++ and make
RUN apk add g++ make
RUN bundle install
RUN bundle update fastlane
